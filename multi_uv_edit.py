# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy

from bpy.props import EnumProperty, StringProperty, BoolProperty, IntProperty, FloatVectorProperty, FloatProperty, CollectionProperty
from bpy.types import AddonPreferences


bl_info = {
    "name": "Multi UV Edit",
    "author": "Lukasz Czyz",
    "version": (1, 0),
    "blender": (2, 78, 0),
    "location": "",
    "description": "",
    "warning": "",
    "wiki_url": "",
    "category": "Object",
    }

def remove_vertex_groups(obj, group_name_prefix):
    assert(obj.mode == 'EDIT')
    for vert_group in obj.vertex_groups:
        if vert_group.name.startswith(group_name_prefix):
            bpy.ops.object.vertex_group_set_active(group=vert_group.name)
            bpy.ops.object.vertex_group_remove()

def select_objects(objs):
    for obj in objs:
        obj.select = True

def deselect_objects(objs):
    for obj in objs:
        obj.select = False

def duplicate_objects(context, src_objs):
    bpy.ops.object.select_all(action='DESELECT')

    select_objects(src_objs)

    bpy.ops.object.duplicate()
    dup_objs = context.selected_objects
    bpy.ops.object.select_all(action='DESELECT')

    return dup_objs



class MultiUvEditPreferences(AddonPreferences):
    bl_idname = 'multi_uv_edit'

    vert_group_prefix = StringProperty(
                    name="Vertex Group Prefix",
                    description="",
                    default="__multi_uv_edit_")

    multi_obj_name = StringProperty(
        name="Vertex Group Prefix",
        description="",
        default="__multiUvEdit")

def get_preferences():
    return bpy.context.user_preferences.addons['multi_uv_edit'].preferences

class MultiUvEditPanel(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_label = 'Multi UV Edit'
    bl_context = ''
    bl_category = 'Tools'

    def draw(self, context):
        prefs = get_preferences()

        layout = self.layout
        col = layout.column(align=True)

        col.operator(MultiUvEditEnterOperator.bl_idname)
        col.operator(MultiUvEditExitOperator.bl_idname)

class MultiUvEditEnterOperator(bpy.types.Operator):
    """Select duplicate vertices"""
    bl_idname = 'obj.multi_uv_edit'
    bl_label = 'Multi UV Edit Enter'
    # bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object.mode == 'OBJECT'

    def validateContext(self, context):
        if len(context.selected_objects) < 2:
            return 'Select more than one object'

        for obj in context.selected_objects:
            if obj.type != 'MESH':
                return 'All selected objects must be meshes'

            if obj.hide:
                return 'All selected objects must be visible'

        return True

    def execute(self, context):
        validate_result = self.validateContext(context)
        if isinstance(validate_result, str):
            self.report({'ERROR'}, validate_result)
            return {'CANCELLED'}

        prefs = get_preferences()
        source_objs = context.selected_objects

        for obj in source_objs:
            context.scene.objects.active = obj

            bpy.ops.object.mode_set(mode='EDIT')
            remove_vertex_groups(obj, prefs.vert_group_prefix)

            bpy.ops.mesh.reveal()
            bpy.ops.mesh.select_all(action='SELECT')

            vert_group = obj.vertex_groups.new(name=prefs.vert_group_prefix + obj.name)
            bpy.ops.object.mode_set(mode='OBJECT')
            vert_group.add(range(len(obj.data.vertices)), 1.0, 'ADD')

        bpy.ops.object.select_all(action='DESELECT')

        dup_objs = duplicate_objects(context, source_objs)
        select_objects(dup_objs)
        dup_active = dup_objs[0]
        context.scene.objects.active = dup_active
        bpy.ops.object.join()

        dup_active.name = prefs.multi_obj_name
        bpy.ops.object.mode_set(mode='EDIT')

        return {'FINISHED'}


class MultiUvEditExitOperator(bpy.types.Operator):
    """Select duplicate vertices"""
    bl_idname = 'obj.multi_uv_exit'
    bl_label = 'Multi UV Edit Exit'

    # bl_options = {'REGISTER', 'UNDO'}
    @classmethod
    def poll(cls, context):
        prefs = get_preferences()
        return context.active_object.mode == 'EDIT' and context.active_object.name == prefs.multi_obj_name

    def validateContext(self, context):
        prefs = get_preferences()
        multi_obj = context.scene.objects.active
        assert (multi_obj.mode == 'EDIT')

        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='SELECT')

        error_msg = None

        for vert_group in multi_obj.vertex_groups:
            if vert_group.name.startswith(prefs.vert_group_prefix):
                orig_name = vert_group.name[len(prefs.vert_group_prefix):]

                if orig_name not in context.scene.objects:
                    error_msg = 'One of original objects not found in the scene - cancelling'
                    break

                orig_obj = context.scene.objects[orig_name]
                if orig_obj not in context.selected_objects:
                    error_msg = 'One of original objects not visible - cancelling'
                    break

        bpy.ops.object.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode='EDIT')

        if error_msg is not None:
            return error_msg

        return True

    def execute(self, context):
        validate_result = self.validateContext(context)
        if isinstance(validate_result, str):
            self.report({'ERROR'}, validate_result)
            return {'CANCELLED'}

        prefs = get_preferences()
        multi_obj = context.scene.objects.active

        bpy.ops.mesh.reveal()
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode='OBJECT')

        for idx, vert_group in enumerate(multi_obj.vertex_groups):
            if len(vert_group.name) <= len(prefs.vert_group_prefix) or not vert_group.name.startswith(prefs.vert_group_prefix):
                continue

            orig_name = vert_group.name[len(prefs.vert_group_prefix):]
            orig_obj = context.scene.objects[orig_name]

            bpy.ops.object.select_all(action='DESELECT')
            # multi_dup = duplicate_objects(context, [multi_obj])[0]

            # if idx == 1:
            #     return {'FINISHED'}

            context.scene.objects.active = multi_obj
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.object.vertex_group_set_active(group=vert_group.name)
            bpy.ops.object.vertex_group_select()
            bpy.ops.mesh.separate(type='SELECTED')
            # bpy.ops.mesh.select_all(action='INVERT')
            # bpy.ops.mesh.delete(type='VERT')

            bpy.ops.object.mode_set(mode='OBJECT')
            deselect_objects([multi_obj])

            assert(len(context.selected_objects) == 1)
            sep_obj = context.selected_objects[0]

            context.scene.objects.active = orig_obj
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.reveal()
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.delete(type='VERT')

            bpy.ops.object.mode_set(mode='OBJECT')
            select_objects([orig_obj, sep_obj])

            assert(context.scene.objects.active == orig_obj)
            bpy.ops.object.join()

            bpy.ops.object.mode_set(mode='EDIT')
            remove_vertex_groups(orig_obj, prefs.vert_group_prefix)
            bpy.ops.object.mode_set(mode='OBJECT')

        bpy.ops.object.select_all(action='DESELECT')
        select_objects([multi_obj])
        bpy.ops.object.delete()

        return {'FINISHED'}

def register():
    bpy.utils.register_class(MultiUvEditPreferences)
    bpy.utils.register_class(MultiUvEditEnterOperator)
    bpy.utils.register_class(MultiUvEditExitOperator)
    bpy.utils.register_class(MultiUvEditPanel)

def unregister():
    bpy.utils.unregister_class(MultiUvEditPanel)
    bpy.utils.unregister_class(MultiUvEditExitOperator)
    bpy.utils.unregister_class(MultiUvEditEnterOperator)
    bpy.utils.unregister_class(MultiUvEditPreferences)
