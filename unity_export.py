import bpy

from bpy.props import EnumProperty, StringProperty, BoolProperty, IntProperty, FloatVectorProperty, FloatProperty, CollectionProperty
from bpy.types import AddonPreferences
from mathutils import Vector


bl_info = {
	"name": "Unity Export",
	"author": "Lukasz Czyz",
	"version": (1, 0),
	"blender": (2, 78, 0),
	"location": "",
	"description": "",
	"warning": "",
	"wiki_url": "",
	"category": "Object",
	}

def duplicate_objects(context, src_objs):
	bpy.ops.object.select_all(action='DESELECT')
	
	for obj in src_objs:
		obj.select = True
		
	bpy.ops.object.duplicate()
	dup_objs = context.selected_objects
	dup_active = context.scene.objects.active
	bpy.ops.object.select_all(action='DESELECT')
	
	return dup_active, dup_objs

def select_objects(objs):
	for obj in objs:
		obj.select = True
			
def add_name_suffix(objs, suffix):
	for obj in objs:
		obj.name = obj.name + suffix
	
def remove_name_suffix(objs, suffix):
	for obj in objs:
		suffix_idx = obj.name.find(suffix)
		
		if suffix_idx == -1:
			raise RuntimeError('Expected substring not found')
		obj.name = obj.name[0: suffix_idx]
	
class UnityExportPreferences(AddonPreferences):
	bl_idname = 'unity_export'
	
	apply_transform = BoolProperty(
						name="Apply Tranfsorm",
						description="",
						default=True)
	

class UnityExportPanel(bpy.types.Panel):
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'TOOLS'
	bl_label = 'Unity Export'
	bl_context = ''
	bl_category = 'Tools'

	@classmethod
	def poll(cls, context):
		return context.active_object.mode == 'OBJECT'

	def draw(self, context):
		prefs = get_preferences()

		layout = self.layout
		col = layout.column(align=True)

		col.prop(prefs, "apply_transform")
		col.separator()
		col.operator(UnityExportOperator.bl_idname)
		
def get_preferences():
	return bpy.context.user_preferences.addons['unity_export'].preferences

def find_common_ancestor(objs):
	processed = set()
	
	common_ancestor = None
	
	for obj in objs:
		if obj in processed:
			continue
			
		ancestor = obj
		
		while ancestor.parent is not None and ancestor.parent in objs:
			processed.add(ancestor)
			ancestor = ancestor.parent
			
		if common_ancestor is None:
			common_ancestor = ancestor
			assert common_ancestor in objs
		else:
			if common_ancestor != ancestor:
				return None
				
	return common_ancestor
	
		
class UnityExportOperator(bpy.types.Operator):
	"""Select duplicate vertices"""
	bl_idname = 'obj.unity_export'
	bl_label = 'Unity Export'
	# bl_options = {'REGISTER', 'UNDO'}

	filepath = StringProperty(subtype="FILE_PATH")
	
	@classmethod
	def poll(cls, context):
		return context.active_object.mode == 'OBJECT'
		
	def execute(self, context):
		tmp_name_suffix = '__unity_export_tmp_obj__'
		prefs = get_preferences()
		source_objs = context.selected_objects

		common_ancestor = find_common_ancestor(source_objs)
		
		if common_ancestor is None:
			self.report({'ERROR'}, "Selected objects don't have common ancestor")
			return {'CANCELLED'}
			
		context.scene.objects.active = common_ancestor
			
		add_name_suffix(source_objs, tmp_name_suffix)
		dup_active, dup_objs = duplicate_objects(context, source_objs)
		remove_name_suffix(dup_objs, tmp_name_suffix)
		
		bpy.ops.object.select_all(action='DESELECT')
		
		select_objects(dup_objs)
		
		bpy.context.space_data.cursor_location = Vector((0.0, 0.0, 0.0))
		# bpy.ops.view3d.snap_cursor_to_selected()
		context.space_data.pivot_point = 'CURSOR'
			
		bpy.ops.transform.rotate(value=-1.5708, axis=(1, 0, 0), constraint_axis=(True, False, False), constraint_orientation='GLOBAL')
		
		bpy.ops.object.make_single_user(type='SELECTED_OBJECTS', obdata=True)
		
		if not prefs.apply_transform:
			bpy.ops.object.select_all(action='DESELECT')
			dup_active.select = True
			
		bpy.ops.object.transform_apply(location=False, rotation=True, scale=True)

		bpy.ops.object.select_all(action='DESELECT')
		dup_active.select = True
		
		# dup_active.rotation_euler[0] = 1.5708
		bpy.ops.transform.rotate(value=1.5708, axis=(1, 0, 0), constraint_axis=(True, False, False), constraint_orientation='GLOBAL')
		
		select_objects(dup_objs)
		bpy.ops.export_scene.fbx(filepath=self.filepath, use_selection=True, bake_anim=False, axis_forward='Y', axis_up='Z')
		
		bpy.ops.object.delete()
		
		remove_name_suffix(source_objs, tmp_name_suffix)
		
		return {'FINISHED'}

	def invoke(self, context, event):
		context.window_manager.fileselect_add(self)
		return {'RUNNING_MODAL'}

def register():
	bpy.utils.register_class(UnityExportPreferences)
	bpy.utils.register_class(UnityExportOperator)
	bpy.utils.register_class(UnityExportPanel)

def unregister():
	bpy.utils.unregister_class(UnityExportPanel)
	bpy.utils.unregister_class(UnityExportOperator)
	bpy.utils.unregister_class(UnityExportPreferences)
