import math

import bpy

from bpy_extras.anim_utils import bake_action
from bpy.props import EnumProperty, StringProperty, BoolProperty, IntProperty, FloatVectorProperty, FloatProperty, CollectionProperty
from bpy.types import AddonPreferences
from mathutils import Vector


bl_info = {
	"name": "Bake Follow Path",
	"author": "Lukasz Czyz",
	"version": (1, 0),
	"blender": (2, 78, 0),
	"location": "",
	"description": "",
	"warning": "",
	"wiki_url": "",
	"category": "Object",
	}

def duplicate_objects(context, src_objs):
	bpy.ops.object.select_all(action='DESELECT')
	
	for obj in src_objs:
		obj.select = True
		
	bpy.ops.object.duplicate()
	dup_objs = context.selected_objects
	dup_active = context.scene.objects.active
	bpy.ops.object.select_all(action='DESELECT')
	
	return dup_active, dup_objs

def select_objects(objs):
	for obj in objs:
		obj.select = True
		
def deselect_objects(objs):
	for obj in objs:
		obj.select = False
			
def add_name_suffix(objs, suffix):
	for obj in objs:
		obj.name = obj.name + suffix
	
def remove_name_suffix(objs, suffix):
	for obj in objs:
		suffix_idx = obj.name.find(suffix)
		
		if suffix_idx == -1:
			raise RuntimeError('Expected substring not found')
		obj.name = obj.name[0: suffix_idx]
	
class BakeFollowPathPreferences(AddonPreferences):
	bl_idname = 'bake_follow_path'
	
	apply_transform = BoolProperty(
						name="Apply Tranfsorm",
						description="",
						default=True)
	

class BakeFollowPathPanel(bpy.types.Panel):
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'TOOLS'
	bl_label = 'Bake Follow Path'
	bl_context = ''
	bl_category = 'Tools'

	@classmethod
	def poll(cls, context):
		return context.active_object.mode == 'OBJECT'

	def draw(self, context):
		prefs = get_preferences()

		layout = self.layout
		col = layout.column(align=True)

		col.operator(BakeFollowPathOperator.bl_idname)
		
def get_preferences():
	return bpy.context.user_preferences.addons['bake_follow_path'].preferences

def active_obj_reset(active):
	active.constraints.clear()
	active.animation_data_clear()
	active.location = (0.0, 0.0, 0.0)
	active.rotation_euler = (math.radians(90.0), 0.0, 0.0)
			
class BakeFollowPathOperator(bpy.types.Operator):
	"""Select duplicate vertices"""
	bl_idname = 'obj.bake_follow_path'
	bl_label = 'Bake Follow Path'
	# bl_options = {'REGISTER', 'UNDO'}

	filepath = StringProperty(subtype="FILE_PATH")
	
	@classmethod
	def poll(cls, context):
		return context.active_object.mode == 'OBJECT'
		
	def execute(self, context):
		tmp_name_suffix = '__bake_follow_path_tmp_obj__'
		prefs = get_preferences()
		
		source_objs = context.selected_objects
		active = context.scene.objects.active
		
		deselect_objects(source_objs)
		select_objects([active])
		
		for obj in source_objs:
			if obj == active:
				continue
				
			if obj.type != 'CURVE':
				continue
				
			active_obj_reset(active)
			action_name = obj.name + 'Action'
			
			action_old = bpy.data.actions.get(action_name)
			if action_old is not None:
				action_old.name = 'crap'
				action_old.use_fake_user = False

			constraint = active.constraints.new('FOLLOW_PATH');
			
			constraint.use_curve_follow = True
			constraint.target = obj
			
			action = bake_action(context.scene.frame_start, context.scene.frame_end, frame_step=1, only_selected=True, do_pose=False, do_object=True, do_visual_keying=True, do_constraint_clear=False, do_parents_clear=False, do_clean=False, action=None)
			  
			action.name = action_name
			action.use_fake_user = True
			
		active_obj_reset(active)
		
		return {'FINISHED'}


def register():
	bpy.utils.register_class(BakeFollowPathPreferences)
	bpy.utils.register_class(BakeFollowPathOperator)
	bpy.utils.register_class(BakeFollowPathPanel)

def unregister():
	bpy.utils.unregister_class(BakeFollowPathPanel)
	bpy.utils.unregister_class(BakeFollowPathOperator)
	bpy.utils.unregister_class(BakeFollowPathPreferences)
