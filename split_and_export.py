import bpy

from bpy.props import EnumProperty, StringProperty, BoolProperty, IntProperty, FloatVectorProperty, FloatProperty, CollectionProperty
from bpy.types import AddonPreferences


bl_info = {
	"name": "Split And Export",
	"author": "Lukasz Czyz",
	"version": (1, 0),
	"blender": (2, 78, 0),
	"location": "",
	"description": "",
	"warning": "",
	"wiki_url": "",
	"category": "Object",
	}

def duplicate_object(context, src_obj):
	bpy.ops.object.select_all(action='DESELECT')
	src_obj.select = True
	context.scene.objects.active = src_obj
	bpy.ops.object.duplicate()
	dup_obj = bpy.context.object
	bpy.ops.object.select_all(action='DESELECT')
	return dup_obj


class SplitPreferences(AddonPreferences):
	bl_idname = 'split_and_export'
	
	split_suffix = StringProperty(
					name="Split Suffix",
					description="",
					default=".LP")
					
	vert_group_prefix = StringProperty(
					name="Vertex Group Prefix",
					description="",
					default="__split_")
					
	subdiv_level = IntProperty(
					name="Subdivide Level",
					description="",
					default=0,
					min=0)

def get_preferences():
	return bpy.context.user_preferences.addons['split_and_export'].preferences
					
class SplitPanel(bpy.types.Panel):
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'TOOLS'
	bl_label = 'Split And Export'
	bl_context = ''
	bl_category = 'Tools'

	@classmethod
	def poll(cls, context):
		return context.active_object is not None and context.active_object.mode == 'OBJECT'

	def draw(self, context):
		prefs = get_preferences()

		layout = self.layout
		col = layout.column(align=True)

		col.prop(prefs, "vert_group_prefix")
		col.prop(prefs, "split_suffix")
		col.separator()
		col.prop(prefs, "subdiv_level")
		col.separator()
		col.operator(SplitOperator.bl_idname)
	
class SplitOperator(bpy.types.Operator):
	"""Select duplicate vertices"""
	bl_idname = 'obj.split_and_export'
	bl_label = 'Split'
	bl_options = {'UNDO'}

	filepath = StringProperty(subtype="FILE_PATH")
	
	def execute(self, context):
		no_bake_name = 'noBake'
		prefs = get_preferences()
		source_obj = context.scene.objects.active

		source_copy = duplicate_object(context, source_obj)
		context.scene.objects.active = source_copy
		
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.reveal()
		bpy.ops.mesh.select_all(action='SELECT')
		
		if prefs.subdiv_level > 0:
			bpy.ops.mesh.subdivide(number_cuts=prefs.subdiv_level, smoothness=0)
		bpy.ops.object.mode_set(mode='OBJECT')
		
		dup_objs = []
		
		obj_for_other = duplicate_object(context, source_copy)
		obj_for_other.name = 'other_geom' + prefs.split_suffix
		
		context.scene.objects.active = obj_for_other
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_all(action='DESELECT')
		bpy.ops.object.mode_set(mode='OBJECT')	
			
		for i in range(0, len(source_copy.vertex_groups)):
			group = source_copy.vertex_groups[i]
			
			if len(group.name) <= len(prefs.vert_group_prefix) or not group.name.startswith(prefs.vert_group_prefix):
				continue
			
			# Process other geometry
			context.scene.objects.active = obj_for_other
			bpy.ops.object.mode_set(mode='EDIT')
			bpy.ops.object.vertex_group_set_active(group=group.name)
			bpy.ops.object.vertex_group_select()
			bpy.ops.object.mode_set(mode='OBJECT')
		
			dup_obj = duplicate_object(context, source_copy)
			
			submesh_name = group.name[len(prefs.vert_group_prefix):]
			name_suffix = prefs.split_suffix if submesh_name != no_bake_name else ''
			dup_obj.name = submesh_name + name_suffix
			
			context.scene.objects.active = dup_obj
			bpy.ops.object.mode_set(mode='EDIT')
			
			bpy.ops.mesh.select_all(action='DESELECT')
			bpy.ops.object.vertex_group_set_active(group=group.name)
			bpy.ops.object.vertex_group_select()
			bpy.ops.mesh.select_all(action='INVERT')
			bpy.ops.mesh.delete(type='VERT')
			
			bpy.ops.object.mode_set(mode='OBJECT')
			
			dup_objs.append(dup_obj)
			
		# Process other geometry
		context.scene.objects.active = obj_for_other
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.delete(type='VERT')
		bpy.ops.object.mode_set(mode='OBJECT')
		
		bpy.ops.object.select_all(action='DESELECT')
		
		objs_to_del = []
		objs_to_del.append(source_copy)
		
		if len(obj_for_other.data.vertices) == 0:
			objs_to_del.append(obj_for_other)
		else:
			dup_objs.append(obj_for_other)
			
		dup_tmp = []
		for obj in dup_objs:
			if len(obj.data.vertices) == 0:
				objs_to_del.append(obj)
			else:
				dup_tmp.append(obj)
			
		dup_objs = dup_tmp
			
		bpy.ops.object.select_all(action='DESELECT')
		for obj in objs_to_del:
			obj.select = True
		
		bpy.ops.object.delete()
		
		for obj in dup_objs:
			obj.select = True
		# context.scene.objects.active = source_obj
		
		return {'FINISHED'}

	# def invoke(self, context, event):
		# context.window_manager.fileselect_add(self)
		# return {'RUNNING_MODAL'}

def register():
	bpy.utils.register_class(SplitPreferences)
	bpy.utils.register_class(SplitOperator)
	bpy.utils.register_class(SplitPanel)

def unregister():
	bpy.utils.unregister_class(SplitPanel)
	bpy.utils.unregister_class(SplitOperator)
	bpy.utils.unregister_class(SplitPreferences)
