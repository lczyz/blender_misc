import os
import bpy
from subprocess import call

bl_info = {
    "name": "Multi OpenGL Render",
    "author": "glukoz",
    "version": (0, 5),
    "blender": (2, 77, 0),
    "location": "",
    "description": "Render OpenGL from many cameras at the same time.",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Render"}

class MultiOglRenderOperator(bpy.types.Operator):
    bl_idname = "wm.rendrer_opengl_multiple"
    bl_label = "Multi OpenGL Render"

    _timer = None

    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'

    def modal(self, context, event):
        if event.type == 'ESC':
            return self.cancel(context)

        if event.type == 'TIMER':
            renderName = str(self.renderNum) + '.png'

            context.scene.render.filepath = os.path.join(self.savePath, renderName)
            self.renderedFiles.append(context.scene.render.filepath)

            context.window_manager.event_timer_remove(self._timer)
            bpy.ops.render.opengl(write_still=True)
            self._timer = context.window_manager.event_timer_add(1, context.window)

            self.renderNum += 1

            if len(self.cameras) > 0:
                self.switchToNextCamera(context)
            else:
                if len(self.renderedFiles) > 1:
                    try:
                        call(['convert'] + self.renderedFiles + ['+append'] + [os.path.join(self.savePath, 'combined.png')])
                    except:
                        print('Convert failed')

                return self.cancel(context)

        return {'PASS_THROUGH'}

    def switchToNextCamera(self, context):
        camObj = self.cameras[0]
        del self.cameras[0]

        context.scene.objects.active = camObj
        bpy.ops.view3d.object_as_camera()

    def execute(self, context):
        self.renderedFiles = []
        self.savePath = context.scene.render.filepath
        self.renderNum = 1

        self.cameras = []
        for obj in context.selected_objects:
            if obj.type == 'CAMERA':
                self.cameras.append(obj)

        if len(self.cameras) == 0:
            return {'CANCELLED'}

        self.cameras = sorted(self.cameras, key=lambda obj: obj.name)
        self.switchToNextCamera(context)

        context.window_manager.modal_handler_add(self)
        self._timer = context.window_manager.event_timer_add(1, context.window)
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        context.scene.render.filepath = self.savePath
        context.window_manager.event_timer_remove(self._timer)
        return {'CANCELLED'}


def register():
    bpy.utils.register_class(MultiOglRenderOperator)


def unregister():
    bpy.utils.unregister_class(MultiOglRenderOperator)
